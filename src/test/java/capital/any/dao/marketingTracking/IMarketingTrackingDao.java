package capital.any.dao.marketingTracking;

import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingTrackingDao extends capital.any.dao.base.marketingTracking.IMarketingTrackingDao {
	
	public static final String GET_TRACKING_AFTER_LOGIN = 
			" select " +
				" mt.* " +
			" from " +
				" marketing_tracking mt, " +
			    " logins lo " +
			" where " +
				" lo.MARKETING_TRACKING_ID = mt.ID " +
			    " and lo.id = (SELECT " + 
								" max(l.id) " +
							" FROM " + 
								" users u, " +
								" logins l " +
							" where" +
								" u.ID = l.USER_ID " +
								" and u.EMAIL like :userEmail " +
							" group by " +
								" l.USER_ID) ";
	
	public static final String GET_TRACKING_BY_ATTRIBUTION = 
			" SELECT " +
				" mt.* " +
			" FROM " +
				" marketing_tracking mt, " +
				" marketing_attribution ma " +
			" WHERE " +
				" mt.id = ma.tracking_id " +
				" and ma.table_id = :tableId" +
				" and ma.user_id = :userId ";

	MarketingTracking getTrackingAfterLogin(String email);
	
	MarketingTracking getTrackingByAttribution(MarketingAttribution marketingAttribution);
}
