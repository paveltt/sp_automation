package capital.any.dao.marketingTracking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("MarketingTrackingDaoAutomation")
public class MarketingTrackingDao extends capital.any.dao.base.marketingTracking.MarketingTrackingDao implements IMarketingTrackingDao {

	@Autowired
	private MarketingTrackingMapper marketingTrackingMapper;
	
	@Override
	public MarketingTracking getTrackingAfterLogin(String email) {
		SqlParameterSource namedParameters = 
			new MapSqlParameterSource("userEmail", email);
		return jdbcTemplate.queryForObject(GET_TRACKING_AFTER_LOGIN, namedParameters, marketingTrackingMapper);
	}

	@Override
	public MarketingTracking getTrackingByAttribution(MarketingAttribution marketingAttribution) {
		SqlParameterSource namedParameters = 
			new MapSqlParameterSource("userId", marketingAttribution.getUserId())
				.addValue("tableId", marketingAttribution.getTableId());
		return jdbcTemplate.queryForObject(GET_TRACKING_BY_ATTRIBUTION, namedParameters, marketingTrackingMapper);
	}

}
