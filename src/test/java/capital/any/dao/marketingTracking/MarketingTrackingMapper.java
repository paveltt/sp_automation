package capital.any.dao.marketingTracking;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class MarketingTrackingMapper extends MapperBase implements RowMapper<MarketingTracking> {

	@Override
	public MarketingTracking mapRow(ResultSet rs, int rowNum) throws SQLException {
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setId(rs.getLong("id"));
		marketingTracking.setCampaignId(rs.getLong("campaign_id"));
		marketingTracking.setQueryString(rs.getString("query_string"));
		marketingTracking.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		marketingTracking.setAbName(rs.getString("ab_name"));
		marketingTracking.setAbValue(rs.getString("ab_value"));
		return marketingTracking;
	}
	
	public MarketingTracking mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
