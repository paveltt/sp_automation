package capital.any.autotest.investment;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import capital.any.testng.util.BaseTest;
import junit.framework.Assert;

/**
 * 
 * @author eyal.ohana
 *
 */
public class Investment extends BaseTest {

	@Parameters({ "domain", "timeout", "userEmail", "userPassword" , "browser" })
	@Test
	public void successInsert(String domain, long timeout, String userEmail, 
			String userPassword, String browser) throws InterruptedException {
		login(domain, timeout, userEmail, userPassword, browser);
		/* press on market's icon */
		WebElement element = driver.findElement(By.cssSelector("body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.inner-section > div > section.product-block.list-single-product-block.risk-level-3.product-type-4.primary.collapsed.visible > div > div.view-toggle-btn.ng-scope.expand"));
		clickElement(browser, element, timeout);
		/* press on continue */									 
		WebElement continueElement1 = driver.findElement(By.cssSelector("body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.inner-section > div > section.product-block.list-single-product-block.risk-level-3.product-type-4.primary.visible.transition.expanded > div > div.table.ng-scope > div > div.table-cell.column.expanded-right-col.fade-in-out-product.ng-scope > button > span"));
		WebElement continueElement2 = driver.findElement(By.cssSelector("body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.inner-section > div > section.product-block.list-single-product-block.risk-level-3.product-type-4.primary.visible.transition.expanded > div > div.table.ng-scope > div > div.table-cell.column.expanded-right-col.fade-in-out-product.ng-scope > button"));
		try {
			clickElement(browser, continueElement1, timeout);
		} catch (InterruptedException e) {
			clickElement(browser, continueElement2, timeout);
		}
		/* fill amount */
		WebElement amountElement = driver.findElement(By.cssSelector("#invAmountInput"));
		amountElement.sendKeys(Keys.chord(Keys.CONTROL, "a"), "5000");
		Thread.sleep(timeout);
		/* press on purchase */
		WebElement purchaseElement = driver.findElement(By.cssSelector("body > div.wrapper.ng-scope > div > section.main-section > div > div > section.product-block.single-product-block.risk-level-3.product-type-4.primary > product-full > div > div > div:nth-child(1) > div.table-cell.left-col > span:nth-child(3) > div.small-block.price-selector-row.ng-scope > div.investment-state.investment-initial.ng-scope > button > span"));
		clickElement(browser, purchaseElement, timeout);
		/* press on message */
		WebElement msgElement = driver.findElement(By.cssSelector("body > div.wrapper.ng-scope > div > section.main-section > div > div > section.product-block.single-product-block.risk-level-3.product-type-4.primary.has-investment > product-full > div > div > div:nth-child(1) > div.table-cell.left-col > span:nth-child(3) > div.small-block.price-selector-row.ng-scope > div.investment-state.investment-purchased.ng-scope > span:nth-child(1)"));
		Assert.assertEquals("Your note was purchased!", msgElement.getText());
	}
}
