package capital.any.autotest.register;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import capital.any.testng.util.BaseTest;

/**
 * @author eran.levy
 *
 */
public class Register extends BaseTest {
	
	@Parameters({ "domain", "timeout", "browser" })
	@Test
	public void register(String domain, long timeout, String browser) throws InterruptedException {
		successRegister(domain, timeout, browser);
		logout(browser, timeout);
	}
	
	@Parameters({ "domain", "timeout", "browser", "shortTimeout" })
	@Test
	public void registerAdditionalInfo(String domain, long timeout, String browser, long shortTimeout) throws InterruptedException {
		successRegister(domain, timeout, browser);
		// day
		WebElement dayElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.day-of-birth-holder > div > single-select:nth-child(1) > ng-include > div.single-select-header-holder.ng-scope > div > span.single-select-selected-option.ng-scope"));
		clickElement(browser, dayElm, shortTimeout);
		WebElement dayElmSelect = driver.findElement(By.cssSelector(
				"#mCSB_2_container > span:nth-child(3)"));
		clickElement(browser, dayElmSelect, shortTimeout);
		// month
		WebElement monthElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.day-of-birth-holder > div > single-select:nth-child(2) > ng-include > div.single-select-header-holder.ng-scope > div > span.single-select-selected-option.ng-scope"));
		clickElement(browser, monthElm, shortTimeout);
		WebElement monthElmSelect = driver.findElement(By.cssSelector(
				"#mCSB_3_container > span:nth-child(3)"));
		clickElement(browser, monthElmSelect, shortTimeout);
		// year
		WebElement yearElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.day-of-birth-holder > div > single-select:nth-child(3) > ng-include > div.single-select-header-holder.ng-scope > div > span.single-select-selected-option.ng-scope"));
		clickElement(browser, yearElm, shortTimeout);		
		WebElement yearElmSelect = driver.findElement(By.cssSelector(
				"#mCSB_4_container > span:nth-child(9)"));
		clickElement(browser, yearElmSelect, shortTimeout);
		// street
		WebElement streetElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.street-address-holder > div > input"));
		streetElm.sendKeys("test");
		// city
		WebElement cityElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.city-holder > div > input"));
		cityElm.sendKeys("test");
		// zip
		WebElement zipElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.postal-code-holder > div > input"));
		zipElm.sendKeys("12312");
		// country
		WebElement countryElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.country-holder > single-select > ng-include > div.single-select-header-holder.ng-scope > div > span.single-select-selected-option.ng-scope"));
		clickElement(browser, countryElm, timeout);
		WebElement countryElmSelect = driver.findElement(By.cssSelector(
				"#mCSB_5_container > span:nth-child(2)"));
		clickElement(browser, countryElmSelect, timeout);
		//submit
		WebElement submitElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div.sign-up-screen-body.ng-scope > div > form > div.button-holder > button"));
		clickElement(browser, submitElm, timeout);
		
		logout(browser, timeout);
	}	
}
