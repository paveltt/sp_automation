package capital.any.autotest.login;

import org.eclipse.jetty.util.thread.strategy.ExecuteProduceConsume;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import capital.any.testng.util.BaseTest;
import junit.framework.Assert;

/**
 * @author eran.levy
 *
 */
public class Login extends BaseTest {
	
//	@FindBy(id = "login:j_username")
//	private WebElement userNameInput;
//	@FindBy(id = "login:j_password")
//	private WebElement passwordInput;
//	@FindBy(css = "#login > div:nth-child(1) > div:nth-child(8) > a.buton_blue.but_sprite.clear.mt10 > span > span")
//	private WebElement loginButton;
	
	@Parameters({ "domain", "timeout", "browser" })
	@Test
	public void checkValidations(String domain, long timeout, String browser) throws InterruptedException {
		driver.manage().window().maximize();
		driver.get(domain);
		// waitForLoad(driver);
		// Wait for X Sec
		Thread.sleep(timeout);
		// press on login
		WebElement element = driver.findElement(By.cssSelector("#header > div > div:nth-child(3) > a"));
		if (browser.equalsIgnoreCase("chrome")) {
			actions.moveToElement(element).click().perform();	
		} else {
			element.click();	
		}
		Thread.sleep(timeout);
		WebElement emailElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(1) > div > input"));
		WebElement passwordElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(2) > div > input"));
		WebElement loginButtonElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > button > span"));
		loginButtonElm.click();
		Thread.sleep(timeout);
		WebElement emailErrMsgElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(1) > div.form-error-msg.ng-binding.ng-scope"));
		WebElement passwordErrMsgElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(2) > div.form-error-msg.ng-binding.ng-scope"));
		//check for mandatory fields validation
		Assert.assertEquals("Mandatory field", emailErrMsgElm.getText());
		Assert.assertEquals("Mandatory field", passwordErrMsgElm.getText());
		// check for email mask on email field
		emailElm.sendKeys("123");
		Assert.assertEquals("Invalid email address", emailErrMsgElm.getText());
		// Wait for 5 Sec
		Thread.sleep(2000);
	}
	
	@Parameters({ "domain", "timeout", "userEmail", "userPassword" , "browser" })
	@Test
	public void successLogin(String domain, long timeout, String userEmail, 
			String userPassword, String browser) throws InterruptedException {
		driver.manage().window().maximize();
		driver.get(domain);
		// waitForLoad(driver);
		// Wait for 5 Sec
		Thread.sleep(timeout);
		// press on login
		WebElement element = driver.findElement(By.cssSelector("#header > div > div:nth-child(3) > a"));
		if (browser.equalsIgnoreCase("chrome")) {
			actions.moveToElement(element).click().perform();	
		} else {
			element.click();	
		}
		// Wait for 5 Sec
		Thread.sleep(timeout);
		// fill login details
		WebElement emailElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(1) > div > input"));
		emailElm.sendKeys(userEmail);
		WebElement passwordElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(2) > div > input"));
		passwordElm.sendKeys(userPassword);
		WebElement loginButtonElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > button > span"));
		loginButtonElm.click();
		Thread.sleep(timeout);
		WebElement logoutButtonElm = driver.findElement(By.cssSelector(
				"#header > div > div:nth-child(3) > button"));
		logoutButtonElm.click();
		Thread.sleep(2000);
	}
	
	@Parameters({ "domain", "timeout", "browser" })
	@Test
	public void failLogin(String domain, long timeout, String browser) throws InterruptedException {
		driver.manage().window().maximize();
		driver.get(domain);
		// waitForLoad(driver);
		// Wait for 5 Sec
		Thread.sleep(timeout);
		// press on login
		WebElement element = driver.findElement(By.cssSelector("#header > div > div:nth-child(3) > a"));
		if (browser.equalsIgnoreCase("chrome")) {
			actions.moveToElement(element).click().perform();	
		} else {
			element.click();	
		}
		// Wait for 5 Sec
		Thread.sleep(timeout);
		// fill login details
		WebElement emailElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(1) > div > input"));
		emailElm.sendKeys("test@anycapital.com");
		WebElement passwordElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(2) > div > input"));
		passwordElm.sendKeys("654321");
		WebElement buttonElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > button > span"));
		buttonElm.click();
		Thread.sleep(timeout);	
		WebElement errorElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > div.errors-holder > div > span"));
		Assert.assertEquals("Incorrect username or password", errorElm.getText());
		Thread.sleep(2000);	
	}
	

}
