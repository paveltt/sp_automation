package capital.any.autotest.marketing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import capital.any.base.enums.Table;
import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.MarketingTracking;
import capital.any.model.base.User;
import capital.any.service.base.user.IUserService;
import capital.any.service.marketingTracking.IMarketingTrackingService;
import capital.any.testng.util.BaseTest;


/**
 * 
 * @author eyal.ohana
 *
 */
public class Marketing extends BaseTest {
	
	@Autowired
	@Qualifier("MarketingTrackingServiceAutomation")
	private IMarketingTrackingService marketingTrackingService;
	@Autowired
	private IUserService userService;

	@Parameters({ "domain", "timeout", "userEmail", "userPassword" , "browser", "marketingUrlIndex", "campaignId" })
	@Test
	public void getTrackingAfterLogin(String domain, long timeout, String userEmail, 
			String userPassword, String browser, String marketingUrlIndex, String campaignId) throws InterruptedException {
		long campId = Long.valueOf(campaignId);
		login(domain + marketingUrlIndex + campId, timeout, userEmail, userPassword, browser);
		MarketingTracking marketingTracking = marketingTrackingService.getTrackingAfterLogin(userEmail);
		logout(browser, timeout);
		Assert.assertEquals(campId, marketingTracking.getCampaignId());
	}
	
	@Parameters({ "domain", "timeout", "userEmail", "userPassword" , "browser", "marketingUrlIndex", "campaignId" })
	@Test
	public void getTrackingAfterRegister(String domain, long timeout, String userEmail, 
			String userPassword, String browser, String marketingUrlIndex, String campaignId) throws InterruptedException {
		long campId = Long.valueOf(campaignId);
		String generateEmail = successRegister(domain + marketingUrlIndex + campId, timeout, browser);
		logout(browser, timeout);
		User user = userService.getUserByEmail(generateEmail);
		MarketingAttribution marketingAttribution = new MarketingAttribution(Table.USERS.getId(), user.getId(), 0, user.getId());
		MarketingTracking marketingTracking = marketingTrackingService.getTrackingByAttribution(marketingAttribution);
		Assert.assertEquals(campId, marketingTracking.getCampaignId());
	}
	
}
