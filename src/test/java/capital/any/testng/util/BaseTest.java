package capital.any.testng.util;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

/**
 * @author eran.levy
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class BaseTest extends AbstractTestNGSpringContextTests {
	
	protected WebDriver driver;
	protected Actions actions;

	@Parameters({ "browser", "driverPath", "driverName" })
	@BeforeClass
	public void initDriver(String browser, @Optional("") String driverPath, @Optional("") String driverName)
			throws Exception {
		System.out.println("You are testing on browser " + browser);
		browser = browser.toLowerCase();
		if (!driverPath.equals("")) {
			System.setProperty(driverName, driverPath);
		}
		if (browser.equals("chrome")) {
			ChromeOptions cOptions = new ChromeOptions();
			cOptions.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation"));
			driver = new ChromeDriver();
		} else if (browser.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", "lib/geckodriver.exe");
			// Now you can Initialize marionette driver to launch firefox
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", true);
			driver = new FirefoxDriver();
		} else {
			throw new RuntimeException("Please create a driver for " + browser);
		}
		PageFactory.initElements(driver, this);
		actions = new Actions(driver);
	}
	
	@AfterSuite
	public void quitDriver() throws Exception {
		driver.quit();
	}
	
	/**
	 * Login
	 * @param domain
	 * @param timeout
	 * @param userEmail
	 * @param userPassword
	 * @param browser
	 * @throws InterruptedException
	 */
	public void login(String domain, long timeout, String userEmail, 
			String userPassword, String browser) throws InterruptedException {
		driver.manage().window().maximize();
		driver.get(domain);
		Thread.sleep(timeout);
		// press on login
		WebElement element = driver.findElement(By.cssSelector("#header > div > div:nth-child(3) > a"));
		clickElement(browser, element, timeout);
		// fill login details
		WebElement emailElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(1) > div > input"));
		emailElm.sendKeys(userEmail);
		WebElement passwordElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > div:nth-child(2) > div > input"));
		passwordElm.sendKeys(userPassword);
		WebElement loginButtonElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > credentials > div > div > form > button > span"));
		clickElement(browser, loginButtonElm, timeout);
//		WebElement hpIconElement = driver.findElement(By.cssSelector("body > div.wrapper.ng-scope > div > section.menu.ng-scope > div > a"));
//		clickElement(browser, hpIconElement, timeout);		
	}
	
	/**
	 * Click element
	 * @param browser
	 * @param element
	 * @param timeout
	 * @throws InterruptedException
	 */
	public void clickElement(String browser, WebElement element, long timeout) throws InterruptedException {
		if (browser.equalsIgnoreCase("chrome")) {
			actions.moveToElement(element).click().perform();	
		} else {
			element.click();	
		}
		Thread.sleep(timeout);
	}
	
	/**
	 * Logout
	 * @param browser
	 * @param timeout
	 * @throws InterruptedException
	 */
	public void logout(String browser, long timeout) throws InterruptedException {
		// press on login
		WebElement element = driver.findElement(By.cssSelector("#header > div > div:nth-child(3) > button"));
		clickElement(browser, element, timeout);
	}
	
	public String successRegister(String domain, long timeout, String browser) throws InterruptedException {
		driver.manage().window().maximize();
		driver.get(domain);
		// waitForLoad(driver);
		Thread.sleep(timeout);
		// press on open account
		WebElement element = driver.findElement(By.cssSelector("#tree-root > li:nth-child(3) > div > a > span"));
		clickElement(browser, element, timeout);
		SimpleDateFormat format = new SimpleDateFormat("yyMMdd.HHmmss");
		Date date = new Date();
		// fill register details
		WebElement firstNameElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > div > div > form > div:nth-child(1) > div.smart-placeholder-wrapper.ng-scope > input"));
		firstNameElm.sendKeys("test");
		WebElement lastNameElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > div > div > form > div:nth-child(2) > div.smart-placeholder-wrapper.ng-scope > input"));
		lastNameElm.sendKeys("test");
		WebElement emailElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > div > div > form > div:nth-child(3) > div.smart-placeholder-wrapper.ng-scope > input"));
		String generateEmail = "selenium." + format.format(date) + "@anycapital.com";
		emailElm.sendKeys(generateEmail);
		WebElement phoneElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > div > div > form > div.phone > div.smart-placeholder-wrapper.ng-scope > input"));
		phoneElm.sendKeys("1234567");
		WebElement passwordElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > div > div > form > div:nth-child(5) > div.smart-placeholder-wrapper.ng-scope > input"));
		passwordElm.sendKeys("123456");
		WebElement termsElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > div > div > form > label"));
		clickElement(browser, termsElm, timeout);
		WebElement registerButtonElm = driver.findElement(By.cssSelector(
				"body > div.wrapper.ng-scope > div > section.main-section > div > div > div > div > div > div > form > button"));
		clickElement(browser, registerButtonElm, timeout);
		return generateEmail;
	}
}
