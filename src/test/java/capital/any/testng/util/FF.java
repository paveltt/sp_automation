package capital.any.testng.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class FF {
	
	public static void main(String[] args) throws InterruptedException {    
	   	 //if you didn't update the Path system variable to add the full directory path to the executable as above mentioned then doing this directly through code
	   	 System.setProperty("webdriver.gecko.driver", "lib/geckodriver.exe");
	   	 //Now you can Initialize marionette driver to launch firefox
	   	 DesiredCapabilities capabilities = DesiredCapabilities.firefox();
	   	 capabilities.setCapability("marionette", true);
//	   	 WebDriver driver = new MarionetteDriver(capabilities);    	 
	   	 // Create a new instance of the Firefox driver
	   	 WebDriver driver = new FirefoxDriver();
	   	 // Launch the Online Store Website
	   	 driver.get("http://www.anyoption.com");
	   	 // Print a Log In message to the screen
	   	 System.out.println("Successfully opened the website www.Store.Demoqa.com");
	   	 // Wait for 5 Sec
	   	 Thread.sleep(30000);
	   	 // Close the driver
	   	 driver.quit();
	    }
	
}
