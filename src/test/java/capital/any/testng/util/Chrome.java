package capital.any.testng.util;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Hello world!
 *
 */
public class Chrome {
	public static void main(String[] args) throws InterruptedException {
		// if you didn't update the Path system variable to add the full
		// directory path to the executable as above mentioned then doing this
		// directly through code
		System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
		ChromeOptions cOptions = new ChromeOptions();
		cOptions.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation"));
		// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.anyoption.com");
		// Print a Log In message to the screen
		System.out.println("Successfully opened the website www.Store.Demoqa.com");
		// Wait for 5 Sec
		Thread.sleep(30000);
		// Close the driver
		driver.quit();
	}

}
