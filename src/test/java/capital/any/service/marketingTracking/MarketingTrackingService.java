package capital.any.service.marketingTracking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.marketingTracking.IMarketingTrackingDao;
import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("MarketingTrackingServiceAutomation")
public class MarketingTrackingService extends capital.any.service.base.marketingTracking.MarketingTrackingService implements IMarketingTrackingService {

	@Autowired
	@Qualifier("MarketingTrackingDaoAutomation")
	private IMarketingTrackingDao marketingTrackingDao; 
	
	@Override
	public MarketingTracking getTrackingAfterLogin(String email) {
		return marketingTrackingDao.getTrackingAfterLogin(email);
	}
	
	@Override
	public MarketingTracking getTrackingByAttribution(MarketingAttribution marketingAttribution) {
		return marketingTrackingDao.getTrackingByAttribution(marketingAttribution);
	}

}
