package capital.any.service.marketingTracking;

import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingTrackingService extends capital.any.service.base.marketingTracking.IMarketingTrackingService {
	
	MarketingTracking getTrackingAfterLogin(String email);
	
	MarketingTracking getTrackingByAttribution(MarketingAttribution marketingAttribution);
}
